package com.exaltit.kata.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNull;

import java.math.BigDecimal;

@Getter @NoArgsConstructor @AllArgsConstructor
public class CreatedBankAccountRequest {

    @NonNull
    private BigDecimal initialCredit;
}
