package com.exaltit.kata.exception;

public class NotFoundException extends RuntimeException {

    private final int httpCode;

    public NotFoundException(final String message) {
        super(message);
        this.httpCode = 404;
    }
}
