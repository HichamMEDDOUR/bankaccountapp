package com.exaltit.kata.exception;

public class NegativeValueException extends RuntimeException{

    public NegativeValueException(String message) {
        super(message);
    }
}
