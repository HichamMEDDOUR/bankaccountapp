package com.exaltit.kata.business;

import com.exaltit.kata.domain.*;
import com.exaltit.kata.exception.NegativeValueException;
import com.exaltit.kata.exception.NullValueException;
import com.exaltit.kata.service.BankAccountService;

import java.math.BigDecimal;
import java.util.UUID;

public class BankAccountBusinessImpl implements BankAccountBusiness {

    private BankAccountService bankAccountService;

    public BankAccountBusinessImpl(BankAccountService bankAccountService) {
        this.bankAccountService = bankAccountService;
    }

    @Override
    public BankAccount create(CreatedBankAccountRequest request) {
        validateBankAccount(request);

        return this.bankAccountService.save(new BankAccount(request.getInitialCredit()));
    }

    private static void validateBankAccount(CreatedBankAccountRequest request) {
        if (request == null || request.getInitialCredit() == null) {
            throw new NullValueException("CreatedBankRequest object and InitialCredit must not be null");
        }

        if (request.getInitialCredit().compareTo(BigDecimal.ZERO) < 0) {
            throw new NegativeValueException("InitialCredit must be greater than or equals 0");
        }
    }

    @Override
    public OperationResponse makeOperation(UUID bankAccountId, OperationRequest request) {
        validateOperation(request);
        BankAccount account = this.bankAccountService.findById(bankAccountId);
        Operation operation = new Operation();
        operation.setType(request.getType());
        operation.setAmount(request.getAmount());
        account = account.doOperation(operation);
        this.bankAccountService.save(account);
        Operation createdOperation = account.getOperations().get(account.getOperations().size() - 1);

        return new OperationResponse(createdOperation.getBalance(), request.getAmount(), request.getType(), createdOperation.getCreatedAt());
    }

    private static void validateOperation(OperationRequest request) {
        if (request == null || request.getType() == null || request.getAmount() == null) {
            throw new NullValueException("OperationRequest object, OperationType and OperationAmount must not be null");
        }

        if (request.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
            throw new NegativeValueException("OperationAmount be greater than 0");
        }
    }
}
