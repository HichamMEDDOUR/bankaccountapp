package com.exaltit.kata;

import com.exaltit.kata.business.BankAccountBusiness;
import com.exaltit.kata.business.BankAccountBusinessImpl;
import com.exaltit.kata.domain.BankAccount;
import com.exaltit.kata.domain.CreatedBankAccountRequest;
import com.exaltit.kata.domain.OperationRequest;
import com.exaltit.kata.domain.enumeration.OperationType;
import com.exaltit.kata.exception.InsufficientCreditException;
import com.exaltit.kata.exception.NegativeValueException;
import com.exaltit.kata.exception.NullValueException;
import com.exaltit.kata.service.BankAccountService;
import com.exaltit.kata.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.UUID;

@RunWith(MockitoJUnitRunner.class)
public class BankAccountBusinessImplTest {

    @Mock
    private BankAccountService bankAccountService;

    private BankAccountBusiness bankAccountBusiness;

    private BankAccount currentAccount;

    @Before
    public void init() {
        bankAccountBusiness = new BankAccountBusinessImpl(this.bankAccountService);
        currentAccount = new BankAccount(new BigDecimal("100"));
    }

    @Test
    public void test_deposit_positive_amount() {
        Mockito.when(this.bankAccountService.findById(UUID.fromString("a0601c34-23e7-43b4-9646-fe2f00467323"))).thenReturn(currentAccount);

        OperationRequest request = new OperationRequest(OperationType.DEPOSIT, new BigDecimal("20"));
        Assertions.assertEquals(new BigDecimal("120"), this.bankAccountBusiness.makeOperation(UUID.fromString("a0601c34-23e7-43b4-9646-fe2f00467323"), request).getBalance());
    }

    @Test
    public void test_withdrawal_positive_amount() {
        Mockito.when(this.bankAccountService.findById(UUID.fromString("a0601c34-23e7-43b4-9646-fe2f00467323"))).thenReturn(currentAccount);

        OperationRequest request = new OperationRequest(OperationType.WITHDRAWAL, new BigDecimal("20"));
        Assertions.assertEquals(new BigDecimal("80"), this.bankAccountBusiness.makeOperation(UUID.fromString("a0601c34-23e7-43b4-9646-fe2f00467323"), request).getBalance());
    }

    @Test
    public void test_withdrawal_positive_amount_should_throws_insufficient_exception() {
        Mockito.when(this.bankAccountService.findById(Utils.BANK_ACCOUNT_EXAMPLE_ID)).thenReturn(currentAccount);

        OperationRequest request = new OperationRequest(OperationType.WITHDRAWAL, new BigDecimal("120"));
        InsufficientCreditException insufficientCreditException = Assertions.assertThrows(InsufficientCreditException.class, () -> {
            this.bankAccountBusiness.makeOperation(Utils.BANK_ACCOUNT_EXAMPLE_ID, request);
        });
        Assertions.assertEquals("Insufficient funds. The withdrawal amount must be less than or equal to your current amount", insufficientCreditException.getMessage());
    }

    @Test
    public void test_create_bank_account_when_create_bank_account_request_is_null_should_throws_null_value_exception() {
        NullValueException nullValueException = Assertions.assertThrows(NullValueException.class, () -> {
            this.bankAccountBusiness.create(null);
        });
        Assertions.assertEquals("CreatedBankRequest object and InitialCredit must not be null", nullValueException.getMessage());
    }

    @Test
    public void test_create_bank_account_when_initial_credit_is_null_should_throws_null_value_exception() {
        CreatedBankAccountRequest request = new CreatedBankAccountRequest();

        NullValueException nullValueException = Assertions.assertThrows(NullValueException.class, () -> {
            this.bankAccountBusiness.create(request);
        });
        Assertions.assertEquals("CreatedBankRequest object and InitialCredit must not be null", nullValueException.getMessage());
    }

    @Test
    public void test_create_bank_account_when_credit_is_lass_than_0_should_throws_negative_value_exception() {
        CreatedBankAccountRequest request = new CreatedBankAccountRequest(new BigDecimal("10").negate());

        NegativeValueException negativeValueException = Assertions.assertThrows(NegativeValueException.class, () -> {
            this.bankAccountBusiness.create(request);
        });
        Assertions.assertEquals("InitialCredit must be greater than or equals 0", negativeValueException.getMessage());
    }

    @Test
    public void test_make_operation_when_operation_request_is_null_should_throws_null_value_exception() {
        NullValueException nullValueException = Assertions.assertThrows(NullValueException.class, () -> {
            this.bankAccountBusiness.makeOperation(Utils.BANK_ACCOUNT_EXAMPLE_ID, null);
        });
        Assertions.assertEquals("OperationRequest object, OperationType and OperationAmount must not be null", nullValueException.getMessage());
    }

    @Test
    public void test_make_operation_when_operation_request_type_is_null_should_throws_null_value_exception() {
        OperationRequest request = new OperationRequest(null, new BigDecimal("20"));

        NullValueException nullValueException = Assertions.assertThrows(NullValueException.class, () -> {
            this.bankAccountBusiness.makeOperation(Utils.BANK_ACCOUNT_EXAMPLE_ID, request);
        });
        Assertions.assertEquals("OperationRequest object, OperationType and OperationAmount must not be null", nullValueException.getMessage());
    }

    @Test
    public void test_make_operation_when_operation_request_amount_is_null_should_throws_null_value_exception() {
        OperationRequest request = new OperationRequest(OperationType.DEPOSIT, null);

        NullValueException nullValueException = Assertions.assertThrows(NullValueException.class, () -> {
            this.bankAccountBusiness.makeOperation(Utils.BANK_ACCOUNT_EXAMPLE_ID, request);
        });
        Assertions.assertEquals("OperationRequest object, OperationType and OperationAmount must not be null", nullValueException.getMessage());
    }

    @Test
    public void test_make_operation_when_operation_request_amount_equals_0_should_throws_negative_value_exception() {
        OperationRequest request = new OperationRequest(OperationType.DEPOSIT, BigDecimal.ZERO);

        NegativeValueException negativeValueException = Assertions.assertThrows(NegativeValueException.class, () -> {
            this.bankAccountBusiness.makeOperation(Utils.BANK_ACCOUNT_EXAMPLE_ID, request);
        });
        Assertions.assertEquals("OperationAmount be greater than 0", negativeValueException.getMessage());
    }

    @Test
    public void test_make_operation_when_operation_request_amount_lass_than_0_should_throws_negative_value_exception() {
        OperationRequest request = new OperationRequest(OperationType.DEPOSIT, new BigDecimal("20").negate());

        NegativeValueException negativeValueException = Assertions.assertThrows(NegativeValueException.class, () -> {
            this.bankAccountBusiness.makeOperation(Utils.BANK_ACCOUNT_EXAMPLE_ID, request);
        });
        Assertions.assertEquals("OperationAmount be greater than 0", negativeValueException.getMessage());
    }
}
