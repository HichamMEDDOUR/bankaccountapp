package com.exaltit.kata;

import com.exaltit.kata.domain.BankAccount;
import com.exaltit.kata.domain.Operation;
import com.exaltit.kata.domain.enumeration.OperationType;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class BankAccountTest {

    @Test
    public void when_add_operation_the_latest_operation_of_bank_account_must_equals_to_added_operation(){
        BankAccount bankAccount = new BankAccount(BigDecimal.ZERO);
        Operation depot = new Operation();
        depot.setType(OperationType.DEPOSIT);
        depot.setAmount(new BigDecimal("20"));

        Operation withdrawal = new Operation();
        withdrawal.setType(OperationType.WITHDRAWAL);
        withdrawal.setAmount(new BigDecimal("10"));


        List<Operation> initialOperations = new ArrayList<>();
        initialOperations.add(depot);
        initialOperations.add(withdrawal);
        bankAccount.setOperations(initialOperations);

        Operation operation = new Operation();
        operation.setType(OperationType.DEPOSIT);
        operation.setAmount(new BigDecimal("30"));

        operation.setId(UUID.fromString("b1701c34-23e7-43b4-9646-fe2f00467567"));
        bankAccount = bankAccount.doOperation(operation);
        List<Operation> operations = bankAccount.getOperations();

        Assertions.assertEquals(operation.getId(), operations.get(operations.size() - 1).getId());
    }

    @Test
    public void test_credit_account() {
        BankAccount bankAccount = new BankAccount(BigDecimal.ZERO);
        Operation operation = new Operation();
        operation.setType(OperationType.DEPOSIT);
        operation.setAmount(new BigDecimal("40"));

        Assertions.assertEquals(new BigDecimal("40"), bankAccount.doOperation(operation).getCredit());
    }
}
