package com.exaltit.kata;

import com.exaltit.kata.business.BankAccountBusiness;
import com.exaltit.kata.domain.BankAccount;
import com.exaltit.kata.domain.CreatedBankAccountRequest;
import com.exaltit.kata.domain.Operation;
import com.exaltit.kata.domain.OperationRequest;
import com.exaltit.kata.domain.enumeration.OperationType;
import com.exaltit.kata.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class BankAccountResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private BankAccountBusiness bankAccountBusiness;

    @Test
    public void test_create_bank_account_with_positive_credit() throws Exception  {
        this.mockMvc.perform( MockMvcRequestBuilders
                .post("/v1/bank_accounts")
                .content(asJsonString(new CreatedBankAccountRequest(new BigDecimal("100"))))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void test_create_bank_account_with_null_request_should_be_returned_bad_request() throws Exception  {
        this.mockMvc.perform( MockMvcRequestBuilders
                .post("/v1/bank_accounts")
                .content(asJsonString(null))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void test_create_bank_account_with_credit_null_should_be_returned_bad_request() throws Exception  {
        this.mockMvc.perform( MockMvcRequestBuilders
                .post("/v1/bank_accounts")
                .content(asJsonString(new CreatedBankAccountRequest()))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void test_create_bank_account_with_credit_lass_than_0_should_be_returned_bad_request() throws Exception  {
        this.mockMvc.perform( MockMvcRequestBuilders
                .post("/v1/bank_accounts")
                .content(asJsonString(new CreatedBankAccountRequest(new BigDecimal("100").negate())))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void test_getting_bank_account_by_id_should_return_200_when_is_already_created() throws Exception {
        BankAccount created = this.bankAccountBusiness.create(new CreatedBankAccountRequest(new BigDecimal("100")));
        this.mockMvc.perform( MockMvcRequestBuilders
                .get("/v1/bank_accounts/{id}", created.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void test_getting_bank_account_by_id_should_return_404_when_is_not_found() throws Exception {
        this.mockMvc.perform( MockMvcRequestBuilders
                .get("/v1/bank_accounts/{id}", Utils.BANK_ACCOUNT_EXAMPLE_ID)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void test_deposit_positive_credit_in_specific_existing_bank_account() throws Exception {
        BankAccount created = this.bankAccountBusiness.create(new CreatedBankAccountRequest(new BigDecimal("100")));
        this.mockMvc.perform( MockMvcRequestBuilders
                .post("/v1/bank_accounts/{id}/operation", created.getId())
                .content(asJsonString(new OperationRequest(OperationType.DEPOSIT, new BigDecimal("20"))))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void test_do_operation_with_null_request_should_be_return_bad_request() throws Exception {
        BankAccount created = this.bankAccountBusiness.create(new CreatedBankAccountRequest(new BigDecimal("100")));
        this.mockMvc.perform( MockMvcRequestBuilders
                .post("/v1/bank_accounts/{id}/operation", created.getId())
                .content(asJsonString(null))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void test_deposit_with_credit_lass_than_0_should_be_return_bad_request() throws Exception {
        BankAccount created = this.bankAccountBusiness.create(new CreatedBankAccountRequest(new BigDecimal("100")));
        this.mockMvc.perform( MockMvcRequestBuilders
                .post("/v1/bank_accounts/{id}/operation", created.getId())
                .content(asJsonString(new OperationRequest(OperationType.DEPOSIT, new BigDecimal("20").negate())))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void test_withdrawal_positive_credit_in_specific_existing_bank_account() throws Exception {
        BankAccount created = this.bankAccountBusiness.create(new CreatedBankAccountRequest(new BigDecimal("100")));
        this.mockMvc.perform( MockMvcRequestBuilders
                .post("/v1/bank_accounts/{id}/operation", created.getId())
                .content(asJsonString(new OperationRequest(OperationType.WITHDRAWAL, new BigDecimal("20"))))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void test_withdrawal_with_credit_lass_than_0_should_be_return_bad_request() throws Exception {
        BankAccount created = this.bankAccountBusiness.create(new CreatedBankAccountRequest(new BigDecimal("100")));
        this.mockMvc.perform( MockMvcRequestBuilders
                .post("/v1/bank_accounts/{id}/operation", created.getId())
                .content(asJsonString(new OperationRequest(OperationType.WITHDRAWAL, new BigDecimal("20").negate())))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void test_withdrawal_insufficient_credit_should_be_return_conflict() throws Exception {
        BankAccount created = this.bankAccountBusiness.create(new CreatedBankAccountRequest(new BigDecimal("100")));
        this.mockMvc.perform( MockMvcRequestBuilders
                .post("/v1/bank_accounts/{id}/operation", created.getId())
                .content(asJsonString(new OperationRequest(OperationType.WITHDRAWAL, new BigDecimal("140"))))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isConflict());
    }

    @Test
    public void test_do_operation_with_not_found_bank_account_should_be_return_not_found() throws Exception {
        this.mockMvc.perform( MockMvcRequestBuilders
                .post("/v1/bank_accounts/{id}/operation", Utils.BANK_ACCOUNT_EXAMPLE_ID)
                .content(asJsonString(new OperationRequest(OperationType.DEPOSIT, new BigDecimal("20"))))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void test_get_all_bank_accounts() throws Exception {
        this.bankAccountBusiness.create(new CreatedBankAccountRequest(new BigDecimal("100")));
        this.bankAccountBusiness.create(new CreatedBankAccountRequest(new BigDecimal("200")));

        MvcResult result = this.mockMvc.perform( MockMvcRequestBuilders
                .get("/v1/bank_accounts")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        ObjectMapper mapper = new ObjectMapper();

        List<BankAccount> actual = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<BankAccount>>() {});
        Assertions.assertEquals(actual.size(), 2);
        Assertions.assertEquals(actual.get(0).getCredit().intValue(), new BigDecimal("100").intValue());
        Assertions.assertEquals(actual.get(1).getCredit().intValue(), new BigDecimal("200").intValue());
    }

    @Test
    public void test_get_all_operations_of_specific_bank_account() throws Exception {
        BankAccount bankAccount = this.bankAccountBusiness.create(new CreatedBankAccountRequest(new BigDecimal("100")));

        this.bankAccountBusiness.makeOperation(bankAccount.getId(), new OperationRequest(OperationType.DEPOSIT, new BigDecimal("20")));
        this.bankAccountBusiness.makeOperation(bankAccount.getId(), new OperationRequest(OperationType.WITHDRAWAL, new BigDecimal("40")));

        MvcResult result = this.mockMvc.perform( MockMvcRequestBuilders
                .get("/v1/bank_accounts/{id}/operations", bankAccount.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        ObjectMapper mapper = new ObjectMapper();

        List<Operation> actual = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<Operation>>() {});
        Assertions.assertEquals(actual.size(), 2);
        Assertions.assertEquals(actual.get(0).getAmount().intValue(), new BigDecimal("40").intValue());
        Assertions.assertEquals(actual.get(0).getType(), OperationType.WITHDRAWAL);

        Assertions.assertEquals(actual.get(1).getAmount().intValue(), new BigDecimal("20").intValue());
        Assertions.assertEquals(actual.get(1).getType(), OperationType.DEPOSIT);
    }

    @Test
    public void test_get_all_operations_of_undefined_bank_account_should_be_return_not_found() throws Exception {
        this.mockMvc.perform( MockMvcRequestBuilders
                .get("/v1/bank_accounts/{id}/operations", Utils.BANK_ACCOUNT_EXAMPLE_ID)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
