package com.exaltit.kata.service;

import com.exaltit.kata.domain.BankAccount;
import com.exaltit.kata.entities.BankAccountEntity;
import com.exaltit.kata.exception.NotFoundException;
import com.exaltit.kata.mapper.BankAccountMapper;
import com.exaltit.kata.repository.BankAccountEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class BankAccountServiceImpl implements BankAccountService{

    @Autowired
    private BankAccountEntityRepository repository;

    @Autowired
    private BankAccountMapper bankAccountMapper;

    @Override
    public List<BankAccount> findAll() {
        List<BankAccountEntity> entities = this.repository.findAll();
        return this.bankAccountMapper.toDomainList(entities);
    }

    @Override
    public BankAccount findById(UUID id) {
        BankAccountEntity entity = repository.findById(id).orElseThrow(() -> new NotFoundException(String.format("No resource found for bank account with id %s.", id)));
        return bankAccountMapper.toDomain(entity);
    }

    @Override
    public BankAccount save(BankAccount account) {
        final BankAccountEntity entity = this.bankAccountMapper.toEntity(account);
        final BankAccount saved = this.bankAccountMapper.toDomain(this.repository.save(entity));
        return saved;
    }
}
