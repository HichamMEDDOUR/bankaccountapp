package com.exaltit.kata.entities;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "bank_account")
@Getter @Setter @NoArgsConstructor
public class BankAccountEntity implements Serializable {

    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(columnDefinition = "BINARY(16)")
    @Id
    private UUID id;

    private BigDecimal credit;

    @OneToMany(targetEntity = OperationEntity.class, mappedBy = "bankAccount", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @org.hibernate.annotations.OrderBy(clause = "created_at DESC")
    private List<OperationEntity> operations = new ArrayList<>();

    public void addOperations(final OperationEntity operation) {
        operation.setBankAccount(this);
        this.operations.add(operation);
    }
}
