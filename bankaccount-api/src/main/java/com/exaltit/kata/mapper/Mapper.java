package com.exaltit.kata.mapper;

import java.util.List;

public interface Mapper<T,D> {

  D toDomain(T e);

  List<D> toDomainList(List<T> e);

  T toEntity(D d);
}
