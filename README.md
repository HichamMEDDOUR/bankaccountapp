# Bank Account

## Context
Le but de ce projet est de développer une API REST en archétecture héxagonale en se basant sur l'exemple de compte bancaire client, cette API répond à 3 principales fonctionnalités qui sont les suivantes : 

### User Story 1
Le client a la possibilité de déposer de l'argent sur son compte bancaire

### User Story 2
Le client peut retirer de l'argent s'il a de fonds suffisants sur son compte, sinon un message d'erreur lui sera affiché

### User Story 3
Visualiser l'historique (date, montant, type et le solde) de ses opérations

## Récupération du projet

``` bash
git clone https://gitlab.com/HichamMEDDOUR/bankaccountapp.git
```
## Build le projet avec mvn 

en positionnant sur la racine du projet lancer la commande suivant :

``` bash
mvn clean install
```

Lancer la classe principale BankAccountApiApplication.class

## Swagger

Une fois l'application démarée (par défaut sur le port 8080), aller sur cette URL pour visualiser toutes les routes API avec Swagger

http://localhost:8080/swagger-ui/

**POST**

Dans un premier temps, il faut créer le compte bancaire client en passant un crédit initial > 0 dans le body

http://localhost:8080/v1/bank_accounts 
 
Body :
 
 ```
 {
    "initialCredit": 100
 }
 ```

l'API va retourner un code de reponse 201 qui indique que le compte est bien créé

Voici un exemple de réponse qui sera retourné

 ```
{
"id": "89494b85-f8c9-49ca-bc18-88d6428932e1",
"credit": 100,
"operations": []
}
 ```

**GET**

Retourner tous les comptes bancaires créés

http://localhost:8080/v1/bank_accounts 

**GET**

Retourner les informations d'un compte bancaire en passant un id qui n'existe pas en base de données

http://localhost:8080/v1/bank_accounts/:id

l'API va retourner une erreur 404, car aucun compte n'est lié à cet id

Voici un exemple d'erreur qui sera retourné 

 ```
 {
     "timestamp": "2023-02-24T08:36:32.690+00:00",
     "status": 404,
     "error": "Not Found",
     "message": "No resource found for bank account with id 5cc08449-7ab0-41ba-a4ca-00ce1cdde351.",
     "path": "/v1/bank_accounts/5cc08449-7ab0-41ba-a4ca-ce1cdde351"
 }
 ```

**GET**

Retourner les informations d'un compte bancaire en listant toutes les opérations effectuées on ordre décoissant

http://localhost:8080/v1/bank_accounts/:id

 ```
 {
  "id": "c59d3479-65ed-4b64-a4bb-941546583483",
  "credit": 110,
  "operations": [
    {
      "type": "WITHDRAWAL",
      "amount": 40
    },
    {
      "type": "DEPOSIT",
      "amount": 30
    },
    {
      "type": "DEPOSIT",
      "amount": 20
    }
  ]
}
 ```

**POST**

Dépôt d'un montant négatif sur le compte

http://localhost:8080/v1/bank_accounts/:id/operation

L'id : c'est l'id du compte créé 

Body :

 ```
 {
    "amount": -20,
    "type": "DEPOSIT"
 }
 ```

l'API va retourner une erreur 400 car l'opération de dépôt d'un montant négatif n'est pas possible

Voici un exemple de retour 

 ```
 {
     "timestamp": "2023-02-24T08:25:49.694+00:00",
     "status": 400,
     "error": "Bad Request",
     "message": "Operation amount must be greater than 0",
     "path": "/v1/bank_accounts/5cc08449-7ab0-41ba-a4ca-ce1cdde351ad/operation"
 }
 ```

**POST**

Dépôt d'un montant positif sur le compte créé

http://localhost:8080/v1/bank_accounts/:id/operation

id : c'est l'id du compte créé 

Body :

 ```
 {
    "amount": 20,
    "type": "DEPOSIT"
 }
 ```

l'API devrra retourner un code de response 200 et l'objet opération créé

 ```
{
  "balance": 120,
  "type": "DEPOSIT",
  "description": "Dépôt de 20€ le 04/03/2023 à 02:38:26"
}
 ```

**POST**

Retrait d'un montant négatif sur le compte

http://localhost:8080/v1/bank_accounts/:id/operation

L'id : c'est l'id du compte créé 

Body :

 ```
 {
    "amount": -20,
    "type": "WITHDRAWAL"
 }
 ```

l'API va retourner une erreur 400 car l'opération de retrait un montant négatif n'est pas possible

Voici un exemple de retour 

 ```
 {
     "timestamp": "2023-02-24T08:25:49.694+00:00",
     "status": 400,
     "error": "Bad Request",
     "message": "Operation amount must be greater than 0",
     "path": "/v1/bank_accounts/5cc08449-7ab0-41ba-a4ca-ce1cdde351ad/operation"
 }
 ```

**POST**

Retrait d'un montant positif sur le compte créé et le montant demandé n'est pas disponible sur le compte bancaire

http://localhost:8080/v1/bank_accounts/:id/operation

L'id : c'est l'id du compte créé 

Body :

 ```
 {
    "amount": 120,
    "type": "WITHDRAWAL"
}
 ```

l'API devrra retourner une erreur 409 car nous pouvons pas retirer si on pas assez de fonds sur le compte

Voici un exemple de retour 

 ```
 {
     "timestamp": "2023-02-24T08:34:32.580+00:00",
     "status": 409,
     "error": "Conflict",
     "message": "Insufficient funds. The withdrawal amount must be less than or equal to your current amount",
     "path": "/v1/bank_accounts/5cc08449-7ab0-41ba-a4ca-ce1cdde351ad/operation"
 }
 ```

**POST**

Retrait d'un montant positif sur le compte créé et le montant demandé est bien disponible sur le compte

http://localhost:8080/v1/bank_accounts/:id/operation

L'id : c'est l'id du compte créé 

Body :

 ```
 {
    "amount": 20,
    "type": "WITHDRAWAL"
}
 ```

l'API devrra retourner un code de reponse 201 pour indiquer que l'opération est bien enregistré

Voici un exemple de réponse qui sera retourné :

```
{
"balance": 80,
"type": "WITHDRAWAL",
"description": "Retrait de 20€ le 04/03/2023 à 03:51:22"
}
```

**GET**

Voir toutes les opérations effectués d'un comptes bancaire

L'id : c'est l'id du compte créé 

http://localhost:8080/v1/bank_accounts/:id/operations

l'API devrra retourner toutes les opérations effectuées sur ce compte en ordre de date de création décroissant

Voici un exemple de retour :

```
[
    {
        "type": "WITHDRAWAL",
        "amount": 60
    },
    {
        "type": "DEPOSIT",
        "amount": 50
    },
    {
        "type": "DEPOSIT",
        "amount": 40
    },
    {
        "type": "WITHDRAWAL",
        "amount": 20
    }
]
```